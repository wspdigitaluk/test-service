# Test Service

## Dependencies

The setup script assumes that you have docker-compose and virtualenv installed:

```
$ docker-compose
Define and run multi-container applications with Docker.

Usage:
  docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
  docker-compose -h|--help

... ...
```

## Connecting to remote databases

You will need a file called `test_service/.env` created in your repo with at least
the following keys (You can see a template in `templat.eenv`).

__DO NOT COMMIT YOUR .env file__

```.env
# This tells the app what type of Config object to load (See config.py)
APP_ENV=dev
DB_SQLALCHEMY_URI=
```

## SSH

You may need to open SSH tunnels in the background on the real host via the proxy jump. A script
to do this would look like this (You can use ./scripts/ssh-open-safe.sh for a two database setup)

```
#!/bin/bash
echo Open DB SSH
ssh -N -L 31415:"<DB HOSTNAME>>":5432 jump &
```

Then the corresponding database hostname and port would be (localhost on linux):

```
PG_SERVER_CORP0=host.docker.internal
PG_PORT_CORP0=31415
```

If you needed to connect to more databases you would just open up a
port on say 31416.

## Running the application

Because this is a fully dockerised working environment you _should not_ create a virtual environment for it. Instead
the environment is built inside the docker container defined in `server.test.Dockerfile`.

## Configuring pyCharm Remote interpreter (Optional)

You need to configure your IDE to look at the interpreter inside the container rather than on your host machine.
In order to do this go to `Preferences -> Project: test-service -> Project Interpreter`.
Click on the gear icon and `Add`. Select `Docker Compose`
interpreter and select `docker-compose.yml` as the configuration file.
The service is `app` and `interpeter` is `python`

If you want to use the pytest run configuratiopns you will need to setup a second interpeter that uses `docker-compose.yml` and
`docker-compose.test-ci.yml` as this will spin up the dependency services

## Running with pyCharm

If you want to run the application you can do so via the included run
configurations for pyCharm called "Run Remote Test". This assumes you have
a correct `test_service/.env` file in your repo.

The Swagger API documentation can be found by default on: `http://localhost:8000/api/v1/docs`
which should let you interact with the API.

## Running from Terminal

You can also run the application from the command line by typing:

```
 ./scripts/run-dev.sh
```

## Running the unit tests

You can run the unit tests either using the included pyCharm run configurations
which are:

- Run All Tests: Run all unit tests
- Run Endpoint Tests: Only run tests marked as "endpoint".
These tests tend be slower as they are more functional

Again there is a shell script to run the tests:

```
 ./scripts/run-tests.sh
```

## Application Structure

The application source code is all contained in the directory test_service/app directory. Inside
here are the directories:

- `api` : Here the REST routes and pydantic schemas for the API are present
    - Functions in here should be __lightweight__. Your view function should only really be
    responsible for serialization and parsing query parameters. Anything more complication
    should be implemented in `da`
- `da` : Data access for CRUD operations on the data
- `db` : Contains the `Cluster` implementation and session management
- `models`: SQLAlchemy model definitions
- `tests`: The pytest suite. Also contains test utilities

In the main `app` directory there is the two python files `main.py` which is a click
 command line tool to run the application locally and `run.py` which give uvicorn an importable
 app instance for production/staging/test.

 ## Alembic schema migrations

 The ORM used by the geo-service is `SQLAlchemy` with `alembic` as the framework for schema migrations.
 Alembic needs to be pointed at a database which is an example of `what I have now`, and then
 looking at the model definitions in `.models` as `what do want it to look like`. For example lets add
 a new column to the `Style` class in models/metadata.py. First CD into the same directory as
 `alembic.ini` and set your `.env` file that alembic should look at. This will set the "current"
 database schema in which to compare against your ORM models. Then modify the model in the models
 package and type:

 `./scripts/alembic.sh revision --auto -m my_message`

 You should see the autogenerate figure out what changes need to be made to the database and
 then create a new script in `alembic/versions`. If it fails because `Target database is not up to date`
 then it means the databases you are pointing to have an older schema. So type:

 `./scripts/alembic.sh upgrade head`

 To bring the database up to date. Delete the generated migration file to undo your actions.


## Installing a New Dependency

You shouldn't update the requirements.txt manually because you may miss some dependencies of a package. Instead used the
script ./scripts/install-package.sh <YOUR_PACKAGE> to run pip3 install in the container and update the requirements file
