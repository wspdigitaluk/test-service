#!/bin/sh
set -e
echo upgrading DB...
cd test_service
alembic upgrade head
cd ..
echo launching service...
gunicorn -k uvicorn.workers.UvicornWorker -w 2 "test_service.run:app" --bind=0.0.0.0:8000
