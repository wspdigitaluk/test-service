#!/usr/bin/env sh

set -eu

# Install docker-compose via pip
pip install --no-cache-dir docker-compose pre-commit
docker-compose -v
