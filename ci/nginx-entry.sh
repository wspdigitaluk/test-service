#!/usr/bin/env bash

NGINX_CONF="/etc/nginx/sites-enabled/default"

if grep -q resolver ${NGINX_CONF}
then
    Resolver_IP=$(awk '/^nameserver/{print $2}' /etc/resolv.conf)
    Resolver="resolver ${Resolver_IP} ;"
    sed -i "s/resolver.*$/${Resolver}/g" ${NGINX_CONF}
fi

nginx -g "daemon off;"
