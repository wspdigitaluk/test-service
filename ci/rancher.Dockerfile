FROM python:3.7-slim

RUN apt-get clean \
    && apt-get -y update \
    && apt-get -y install --no-install-recommends python3-dev build-essential \
    && rm -rf /var/lib/apt/lists/*

COPY test_service/requirements.txt /srv/test_service/requirements.txt
WORKDIR /srv/test_service
RUN pip install -r requirements.txt --src /usr/local/src

COPY test_service /srv/test_service
WORKDIR /srv/

COPY ci/app-entry.sh /etc/app-entry.sh

ENV PYTHONPATH /srv/
