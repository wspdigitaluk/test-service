#!/bin/sh
alembic_args="$*"
docker-compose -f docker-compose.yml -f docker-compose.local-db.yml run --rm -w"/var/test-service/test_service/" app alembic $alembic_args
