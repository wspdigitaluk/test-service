#!/bin/sh
export PYYTHONPATH=.
command="$*"

cmd="pip3 install $command && pip3 freeze > test_service/requirements.txt"
echo $cmd
docker-compose -f docker-compose.yml run app bash -c "$cmd"
