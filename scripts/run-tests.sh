#!/bin/sh
export PYYTHONPATH=.
docker-compose -f docker-compose.yml -f docker-compose.test-ci.yml run -T app pytest test_service/app/tests --junitxml=./test-reports/junit.xml
