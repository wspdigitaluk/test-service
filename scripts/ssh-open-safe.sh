#!/bin/bash

if [[ "$TEST_SERVICE" == "" ]]; then
  echo Variable TEST_SERVICE is undefined. Cannot run remotely
  exit 1
fi

echo opening ssh tunnel...
ssh -N -L 1112:"$TEST_SERVICE ":5432 jump &
tunnel_pid=$!
wait-on tcp:31415

read -r -p "Press enter to kill tunnel:"
kill $tunnel_pid
