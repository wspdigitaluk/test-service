FROM python:3.7
COPY test_service/requirements.txt .
RUN pip3 install -r requirements.txt
