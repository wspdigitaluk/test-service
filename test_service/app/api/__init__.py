import dataclasses
import importlib
import pkgutil
from typing import List

from fastapi import APIRouter

_API_VERSIONS: list = []


@dataclasses.dataclass
class ApiVersion:
    version: str
    parent_router: APIRouter
    mount_point: str


def get_apis() -> List[ApiVersion]:
    return _API_VERSIONS


def _import_versions():
    """
    Import the api versions by inspecting the sub-folders in this package. Creating a new API just requires
    a new module named 'vX' which has a 'parent_router' FastAPI router at the top level
    :return:
    """
    for module_info in pkgutil.iter_modules(__path__):
        module = module_info.module_finder.find_module(module_info.name).load_module(module_info.name)
        try:
            router = getattr(module, "parent_router")
            _API_VERSIONS.append(
                ApiVersion(version=module_info.name, parent_router=router, mount_point="/{}".format(module_info.name))
            )
        except AttributeError:
            raise RuntimeError("Failed to import api version '{}'".format(module_info.name))


_import_versions()
