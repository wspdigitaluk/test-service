# flake8: noqa
from fastapi import APIRouter

# We need to import these after parent_router to prevent a circular import
from . import people

parent_router = APIRouter()

parent_router.include_router(people.router, prefix="/people", tags=["test-service People"])
