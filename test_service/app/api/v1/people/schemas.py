from pydantic import BaseModel, Field


class Person(BaseModel):
    first_name: str
    second_name: str
    age: int = Field(..., gt=0)
