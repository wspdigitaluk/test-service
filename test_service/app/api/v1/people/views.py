import logging
from typing import List

from fastapi import APIRouter, Depends

from test_service.app.da.people import get_people
from test_service.app.utils import get_session

from . import schemas

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

router = APIRouter()


@router.get("/", response_model=List[schemas.Person])
def get_all_people(session=Depends(get_session)):
    people = []
    for person in get_people(session):
        people.append(schemas.Person(first_name=person.first_name, second_name=person.second_name, age=person.age))

    return people
