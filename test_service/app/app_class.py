from logging import Logger

from fastapi import FastAPI

from test_service.app.config import Config
from test_service.app.db import Database
from test_service.app.models.example import Person


class Test_ServiceAPI(FastAPI):
    logger: Logger
    config: Config
    db: Database

    def init_db(self, data=True):
        """
        Create the models and any required minimum data for the DB if data=True
        """
        self.db.create_models()
        if data:
            with self.db.session_scope() as session:
                people = []
                for i in range(0, 10):
                    people.append(Person(first_name=f"first{i}", second_name=f"second{i}", age=i * 5 + 5))
                session.add_all(people)
