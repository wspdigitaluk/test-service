import logging
import os

from fastapi import FastAPI
from pydantic import BaseSettings, PostgresDsn

from test_service.app.utils import get_request_id


class AppFilter(logging.Filter):
    def filter(self, record):
        record.request_id = get_request_id()
        return True


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Config(BaseSettings):
    ENVIRONMENT: str = "NONE"
    API_NAME: str = "Test Service"
    API_PREFIX: str = "/api"
    OPENAPI_URL: str = "openapi.json"
    DOCS_URL: str = "docs"
    REDOC_URL: str = "redocs"

    SENTRY_DSN: str = ""
    SENTRY_ENV: str = "dev"

    VERSION: str = "0.0.1"  # TODO: Version with versioneer?

    SQLALCHEMY_DATABASE_URI: PostgresDsn

    class Config:
        extra = "ignore"

    def init_app(self, app: FastAPI):
        """
        Called just after app instantiation by the factory.
        Use this to do some extra dynamic initialisation based on the config chosen.
        For example setting up loggers/directory structure

        DON'T use this for any heavy logic that really should be part of the application code itself

        :param app: The ASGI application
        :return: Nothing
        """


class ConfigDev(Config):
    ENVIRONMENT: str = "dev"

    def init_app(self, app: FastAPI):
        super().init_app(app)
        root = logging.getLogger()
        root.handlers = []
        formatter = logging.Formatter(
            "%(asctime)s - %(process)s - %(request_id)s - %(name)s - %(levelname)s - %(message)s"
        )
        stream_handler = logging.StreamHandler()
        stream_handler.addFilter(AppFilter())
        stream_handler.setLevel(logging.DEBUG)
        stream_handler.setFormatter(formatter)
        root.addHandler(stream_handler)
        root.setLevel(logging.DEBUG)


class ConfigDevLocal(ConfigDev):
    ENVIRONMENT: str = "local"


class ConfigTest(Config):
    ENVIRONMENT: str = "test"
    TESTING: bool = True

    def init_app(self, app: FastAPI):
        super().init_app(app)


_configs = {"dev": ConfigDev, "default": ConfigDev, "test": ConfigTest, "dev-local": ConfigDevLocal}


def from_envvar() -> Config:
    try:
        choice = os.environ["APP_ENV"]
    except KeyError:
        raise KeyError("'APP_ENV' is not set")
    if choice not in _configs:
        msg = "APP_ENV={} is not valid, must be one of {}".format(choice, set(_configs))
        raise ValueError(msg)

    loaded_config = _configs[choice](**os.environ)
    return loaded_config  # type: ignore


def from_kwargs(choice, **fields) -> Config:
    return _configs[choice](**fields)  # type: ignore
