from sqlalchemy.orm import Session

from ..models.example import Person


def get_people(session: Session):
    """
    Returns all the people in the DB
    """
    return session.query(Person).all()
