import logging
from contextlib import contextmanager
from time import sleep, time

from pydantic import PostgresDsn
from sqlalchemy import MetaData
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import Session, sessionmaker

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def _wait_for_up(engine: Engine, sleep_sec: int = 2, timeout: int = 60):
    start = time()
    connected = False
    url_parsed: URL = engine.url
    while (time() - start) < timeout:
        try:
            engine.execute("SELECT 1;")
            connected = True
            break
        except OperationalError:
            logger.debug(f"Could not connect to {url_parsed.host}. Retrying...")
            sleep(sleep_sec)
    if not connected:
        raise RuntimeError(f"Could not connect ot database {url_parsed.host}")


class Database:
    def __init__(self, uri: PostgresDsn, metadata: MetaData):
        self.uri = uri
        self.metadata = metadata
        self.engine = None
        self._session_maker = sessionmaker(class_=Session)

    @property
    def connected(self):
        return self.engine is not None

    def connect(self, timeout=60):
        logger.info("Connecting to database")
        self.engine = create_engine(
            self.uri,
            pool_pre_ping=True,
            pool_size=5,
            max_overflow=10,
            echo=False,
            connect_args={"options": "-c statement_timeout=30000"},
        )
        self._session_maker.configure(bind=self.engine)
        _wait_for_up(self.engine, timeout=timeout)

    def disconnect(self):
        logger.info("Disconnecting from engine")
        self.engine.dispose()

    def create_schema(self):
        if not self.connected:
            raise RuntimeError("Can't create schema on disconnected Database")
        self.engine.execute(f"CREATE SCHEMA IF NOT EXISTS test_service")

    def create_models(self):
        if not self.connected:
            raise RuntimeError("Can't create models on disconnected Database")
        self.create_schema()
        logger.info("Creating models for database")

        self.metadata.create_all(self.engine)

    def drop_models(self):
        if not self.connected:
            raise RuntimeError("Can't drop models on disconnected Database")
        logger.info("Dropping all models for database")
        self.metadata.drop_all(self.engine)

    def session(self):
        if not self.connected:
            raise RuntimeError("Can't get session on disconnected Database")
        return self._session_maker()

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""
        session = self.session()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()
