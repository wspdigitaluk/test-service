"""Setup for the Flask application."""
import asyncio
import logging
from contextlib import contextmanager

import sentry_sdk
from sentry_asgi import SentryMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse

from test_service.app.api import get_apis
from test_service.app.db import Database
from test_service.app.models import md
from test_service.app.utils import RequestContextLogMiddleware

from . import config
from .app_class import Test_ServiceAPI

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

DOMAIN_REGEX = r"(http:\/\/localhost)|(https:\/\/(.*\.apps\.wsp-lattice\.io))"


async def lifespan(app, receive_queue, send_queue) -> None:
    scope = {"type": "lifespan"}
    try:
        await app(scope, receive_queue.get, send_queue.put)
    finally:
        await send_queue.put(None)


async def wait_startup(receive_queue, send_queue, task) -> None:
    await receive_queue.put({"type": "lifespan.startup"})
    message = await send_queue.get()
    assert message["type"] in ("lifespan.startup.complete", "lifespan.startup.failed")
    if message["type"] == "lifespan.startup.failed":
        message = await send_queue.get()
        if message is None:
            task.result()


async def wait_shutdown(receive_queue, send_queue, task) -> None:
    await receive_queue.put({"type": "lifespan.shutdown"})
    message = await send_queue.get()
    if message is None:
        task.result()
    assert message["type"] == "lifespan.shutdown.complete"
    await task


@contextmanager
def app_context(app: Test_ServiceAPI):
    """
    Simulate an applications lifetime events application within a startup/shutdown context
    This is used for CLI tools so that appropriate lifetime events are run as expected

    Borrowed heavily from the Starlette Test Client

    :param app: The Geo-Service App instance
    :return: The application within a lifetime context
    """
    loop = asyncio.get_event_loop()
    send_queue = asyncio.Queue()  # type: asyncio.Queue
    receive_queue = asyncio.Queue()  # type: asyncio.Queue
    task = loop.create_task(lifespan(app, receive_queue, send_queue))
    try:
        loop.run_until_complete(wait_startup(receive_queue, send_queue, task))
        yield app
    finally:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(wait_shutdown(receive_queue, send_queue, task))


def create_app(cfg: config.Config = None) -> Test_ServiceAPI:
    """

    :param cfg: Pass specific Config object to the factory
    :return:
    """
    if cfg is None:
        cfg = config.from_envvar()
    app = Test_ServiceAPI(
        title=cfg.API_NAME,
        version=cfg.VERSION,
        openapi_url=f"{cfg.API_PREFIX}/{cfg.OPENAPI_URL}",
        docs_url=f"{cfg.API_PREFIX}/{cfg.DOCS_URL}",
        redoc_url=f"{cfg.API_PREFIX}/{cfg.REDOC_URL}",
    )
    app.config = cfg

    cfg.init_app(app)

    app.add_middleware(
        CORSMiddleware,
        allow_origin_regex=DOMAIN_REGEX,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.add_middleware(RequestContextLogMiddleware)

    app.db = Database(cfg.SQLALCHEMY_DATABASE_URI, md)

    # Add Sentry if the environment variable is set
    sentry_dsn = cfg.SENTRY_DSN
    if sentry_dsn:
        sentry_sdk.init(dsn=sentry_dsn, environment=cfg.SENTRY_ENV)
    app.add_middleware(SentryMiddleware)

    app.on_event("startup")(app.db.connect)
    app.on_event("shutdown")(app.db.disconnect)

    setup_routes(app)

    return app


def setup_routes(app):
    """Register routes."""

    latest_doc_route = None
    for version in get_apis():
        logger.debug("Mounting API Version: {}".format(version))
        api_app = Test_ServiceAPI(
            title=app.config.API_NAME, openapi_prefix=version.mount_point, version=version.version
        )
        api_app.config = app.config
        api_app.db = app.db

        api_app.include_router(version.parent_router)
        app.mount(version.mount_point, api_app)

        latest_doc_route = version.mount_point + "/" + app.config.DOCS_URL

    # Redirect the root URL endpoint to the latest API docs if we are supplying them
    if app.config.DOCS_URL:
        app.add_route("/", lambda x: RedirectResponse(latest_doc_route))
