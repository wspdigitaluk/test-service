from typing import Any

from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

# See: https://alembic.sqlalchemy.org/en/latest/naming.html#the-importance-of-naming-constraints
convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


md = MetaData(schema="test_service", naming_convention=convention)
ModelBase: Any = declarative_base(metadata=md)
