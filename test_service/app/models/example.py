from sqlalchemy import Column, Integer, String, UniqueConstraint

from . import ModelBase

# See: https://alembic.sqlalchemy.org/en/latest/naming.html#the-importance-of-naming-constraints
convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


class Person(ModelBase):
    """
    An example SQLAlchemy model
    """

    __tablename__ = "people"

    id = Column(Integer(), primary_key=True)
    # The location of the data across the clusters
    first_name = Column(String(128), nullable=False)
    second_name = Column(String(128), nullable=False)

    age = Column(Integer(), nullable=False)

    __table_args__ = (UniqueConstraint("first_name", "second_name"),)
