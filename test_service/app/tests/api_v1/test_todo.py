import pytest
from starlette.testclient import TestClient


@pytest.mark.endpoint
def test_get_people(app_client: TestClient):
    assert len(app_client.get("/v1/people/").json()) == 10
