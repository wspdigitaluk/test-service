from time import sleep
from typing import Dict

import pytest
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from starlette.testclient import TestClient

from test_service.app.config import Config, from_envvar
from test_service.app.models.example import Person

from ..factory import create_app


@pytest.fixture
def test_config(request) -> Config:
    cfg_obj = from_envvar()
    _wait_for_db_live({"primary": cfg_obj.SQLALCHEMY_DATABASE_URI})
    return cfg_obj


@pytest.fixture()
def app_client(test_config: Config) -> TestClient:
    app = create_app(cfg=test_config)
    with TestClient(app) as client:
        app.db.drop_models()
        app.init_db(data=False)

        # Insert some fake data
        with app.db.session_scope() as session:
            people = []
            for i in range(0, 10):
                people.append(Person(first_name=f"first{i}", second_name=f"second{i}", age=i * 5 + 5))
            session.add_all(people)
        yield client


def _wait_for_db_live(uris: Dict[str, str]):
    engines = [create_engine(uri) for uri in uris.values()]
    try:
        while True:
            success = 0
            for engine in engines:
                try:
                    engine.execute("SELECT 1;")
                    success += 1
                except OperationalError:
                    pass
            if success == len(engines):
                break
            else:
                print("Waiting for databases [{}/{}]".format(success, len(engines)))
                sleep(2)
    finally:
        [engine.dispose() for engine in engines]
