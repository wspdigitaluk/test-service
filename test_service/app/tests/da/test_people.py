from test_service.app.da import people


def test_get_all(app_client):
    with app_client.app.db.session_scope() as session:
        assert len(people.get_people(session)) == 10
