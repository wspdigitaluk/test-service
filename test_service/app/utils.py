from contextvars import ContextVar
from uuid import uuid4

from sqlalchemy.orm import Session
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request

REQUEST_ID_CTX_KEY = "request_id"
_request_id_ctx_var: ContextVar[str] = ContextVar(REQUEST_ID_CTX_KEY, default=None)  # type: ignore


def get_request_id() -> str:
    return _request_id_ctx_var.get()


class RequestContextLogMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint):
        request_id = _request_id_ctx_var.set(str(uuid4()))
        response = await call_next(request)
        _request_id_ctx_var.reset(request_id)

        return response


def get_session(request: Request) -> Session:
    session = None
    try:
        session = request.app.db.session()
        yield session
    finally:
        if session:
            session.close()


def get_team(request: Request) -> str:
    team_header = request.headers.get("x-team")
    if team_header is None:
        if request.app.config.TEAM_HEADER_INJECT is not None:
            team_header = request.app.config.TEAM_HEADER_INJECT
    return team_header
