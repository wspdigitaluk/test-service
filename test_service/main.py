import click
import uvicorn
from dotenv import load_dotenv

from test_service.app.factory import app_context, create_app


@click.group()
@click.option("--env", default=".env", help="Which --env file to use")
def cli(env):
    load_dotenv(env)


@cli.command()
@click.option("--reload", is_flag=True)
def run(reload=False):
    if reload:
        uvicorn.run("test_service.run:app", host="0.0.0.0", port=8000, reload=True)
    else:
        app = create_app()
        uvicorn.run(app, host="0.0.0.0", port=8000, reload=False)


@cli.command()
def init_db():
    app = create_app()
    with app_context(app):
        app.init_db()


if __name__ == "__main__":
    cli()  # pylint: disable=E1120
