# flake8: noqa
from __future__ import with_statement

import logging
import sys
from logging.config import fileConfig
from os.path import abspath, dirname
from dotenv import load_dotenv
from alembic import context
from sqlalchemy import engine_from_config, pool

src_path = dirname(dirname(dirname(abspath(__file__))))
sys.path.insert(0, src_path)

from test_service.app.factory import app_context, create_app

USE_TWOPHASE = False

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)
logger = logging.getLogger("alembic.env")



def run_migrations_offline(app):
    """Run migrations in 'offline' mode.
    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.
    Calls to context.execute() here emit the given string to the
    script output.
    """
    # for the --sql use case, run migrations

    raise NotImplementedError("Offline migrations not yet implemented")
    # If we want to do offline .sql migrations then we need to implement this


def include_object(object, name, type_, reflected, compare_to):
    if type_ == "table" and object.schema not in ("test_service", ):
        return False
    return True


def run_migrations_online(app):
    """Run migrations in 'online' mode.
    In this scenario we need to create an Engine
    and associate a connection with the context.
    """
    # for the direct-to-DB use case, start a transaction on all
    # engines, then run all migrations, then commit all transactions.
    rec = {}
    rec["engine"] = engine_from_config(
        {"sqlalchemy.url": app.db.uri}, prefix="sqlalchemy.", poolclass=pool.NullPool
    )
    rec["connection"] = rec["engine"].connect()
    if USE_TWOPHASE:
        rec["transaction"] = rec["connection"].begin_twophase()
    else:
        rec["transaction"] = rec["connection"].begin()

    try:
        app.db.create_schema()
        logger.info("Migrating database")
        context.configure(
            connection=rec["connection"],
            upgrade_token="upgrades",
            downgrade_token="downgrades",
            target_metadata=app.db.metadata,
            include_schemas=True,
            include_object=include_object,
        )
        context.run_migrations()

        if USE_TWOPHASE:
            rec["transaction"].prepare()

        rec["transaction"].commit()
    except:
        rec["transaction"].rollback()
        raise
    finally:
        rec["connection"].close()


load_dotenv()
_app = create_app()
with app_context(_app) as app:
    if context.is_offline_mode():
        run_migrations_offline(app)
    else:
        run_migrations_online(app)
