from dotenv import load_dotenv

from test_service.app.factory import create_app

load_dotenv()
app = create_app()
